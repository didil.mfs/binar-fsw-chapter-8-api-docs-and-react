const moment = require("moment");
const posts = [
  {
    id: 1,
    title: "Lorem ipsum",
    body: "dolor sit amet",
    createdAt: new Date(),
    fromNow: moment(new Date()).fromNow(),
  },
  {
    id: 2,
    title: "Lorem ipsum",
    body: "dolor sit amet",
    createdAt: new Date(),
    fromNow: moment(new Date(2020, 5, 25)).fromNow(),
  },
];

module.exports = {
  index: (req, res) => {
    const locals = {
      data: {
        posts,
      },
      contentName: "Post",
    };

    res.render("pages/dashboard/post", locals);
  },
};
