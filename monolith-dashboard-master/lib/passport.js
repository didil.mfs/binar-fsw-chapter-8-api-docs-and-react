const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const User = require("../models/user");

const authenticate = (username, password, done) => {
  User.findOne({ username }).then((user) => {
    if (!user) return done("User tidak ditemukan", false);
    if (!user.verifyPassword(password))
      return done("Password user tidak cocok", false);

    return done(null, user);
  });
};

passport.use(
  new LocalStrategy(
    { usernameField: "username", passwordField: "password" },
    authenticate
  )
);

passport.serializeUser((user, done) => done(null, user.id));
passport.deserializeUser(async (userId, done) =>
  done(null, await User.findByPk(userId))
);

module.exports = passport;
