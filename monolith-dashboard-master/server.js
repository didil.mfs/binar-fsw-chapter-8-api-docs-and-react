const { urlencoded } = require("express");
const express = require("express");
const app = express();
const { port = 8000 } = process.env;
const expressLayout = require("express-ejs-layouts");

const session = require('express-session');
app.use(
  session({
    secret: 'secret key',
    resave: false,
    saveUninitialized: false,
  })
);

const passport = require("./lib/passport");
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static("public"));
app.set("view engine", "ejs");
app.use(expressLayout);
app.set("layout", "layouts/default");

app.use(urlencoded({ extended: false }));
app.use(express.json());

const setDefault = (req, res, next) => {
  res.locals = { contentName: "Default" };
  next();
};
app.use(setDefault);

const router = require("./router");
app.use(router);

app.listen(port, () => {
  console.log(`Listenin on http://localhost:${port}`);
});
