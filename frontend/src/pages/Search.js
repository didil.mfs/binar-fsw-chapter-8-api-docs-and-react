// import logo from "./logo.svg";
import "./App.css";
import React, { useState } from "react";
// import ReactDOM from "react-dom/client";

function Search() {
  const [inputs, setInputs] = useState({});
  const [submitting, setSubmitting] = useState(false);

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs((values) => ({ ...values, [name]: value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    // console.log(inputs.email);
    // alert("User " + inputs.username + " berhasil dibuat!");
    if (inputs.email === undefined || inputs.username === undefined) {
      setSubmitting(false);
    } else {
      setSubmitting(true);
    }
    // setTimeout(() => {
    //   setSubmitting(false);
    // }, 3000);
  };

  return (
    <div className="App">
      <header className="App-header">
        <h1>Search Player</h1>
        <form onSubmit={handleSubmit}>
          <label>
            Username:
            <input type="text" name="username" value={inputs.username || ""} onChange={handleChange} />
          </label>
          <label>
            Email:
            <input type="text" name="email" value={inputs.email || ""} onChange={handleChange} />
          </label>
          <label>
            Experience:
            <input type="number" name="experience" value={inputs.experience || ""} onChange={handleChange} />
          </label>
          <label>
            Level:
            <input type="number" name="level" value={inputs.level || ""} onChange={handleChange} />
          </label>
          <input type="submit" />
        </form>
        <br></br>
        {
          submitting && "Username: " + inputs.username + "\nEmail: " + inputs.email + "\nExperience: " + inputs.experience + "\nLevel: " + inputs.level
          // && <p>{inputs.username}</p> && <p>{inputs.email}</p>
        }
      </header>
    </div>
  );
}

console.log(Search.handleSubmit);

export default Search;
