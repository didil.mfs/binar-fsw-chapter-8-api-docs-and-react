// import logo from "./logo.svg";
import "./App.css";
import React, { useState } from "react";
// import ReactDOM from "react-dom/client";

function Register() {
  const [inputs, setInputs] = useState({});
  const [submitting, setSubmitting] = useState(false);

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs((values) => ({ ...values, [name]: value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    // console.log(inputs.email);
    // alert("User " + inputs.username + " berhasil dibuat!");
    if (inputs.email === undefined || inputs.username === undefined || inputs.password === undefined) {
      setSubmitting(false);
    } else {
      setSubmitting(true);
    }
    // setTimeout(() => {
    //   setSubmitting(false);
    // }, 3000);
  };

  return (
    <div className="App">
      <header className="App-header">
        <h1>Register Player</h1>
        <form onSubmit={handleSubmit}>
          <label>
            Username:
            <input type="text" name="username" value={inputs.username || ""} onChange={handleChange} />
          </label>
          <label>
            Email:
            <input type="text" name="email" value={inputs.email || ""} onChange={handleChange} />
          </label>
          <label>
            Password:
            <input type="password" name="password" value={inputs.password || ""} onChange={handleChange} />
          </label>
          <input type="submit" />
        </form>
        <br></br>
        {
          submitting && "Username: " + inputs.username + "\nEmail: " + inputs.email + "\nPassword: " + inputs.password
          // && <p>{inputs.username}</p> && <p>{inputs.email}</p>
        }
      </header>
    </div>
  );
}

console.log(Register.handleSubmit);

export default Register;
